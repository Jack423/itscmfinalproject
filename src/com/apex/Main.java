package com.apex;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        Scanner scanner = new Scanner(System.in);
        boolean postSeason = false;

        System.out.println("-------Teams-------\n" +
                "1. Chicago Bears\n" +
                "2. Green Bay Packers\n" +
                "3. Minnesota Vikings\n" +
                "4. Generic Team\n" +
                "-------------------");
        game.playGame(postSeason);

        System.out.println("Season has ended...");
    }
}

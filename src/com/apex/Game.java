package com.apex;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private int quarter = 0, posession;
    private int max, bestTeam, secondBestTeam;
    private int bearsWins = 0, packersWins = 0, vikingsWins = 0, genericWins = 0;
    //public boolean postSeason = false;
    private int[] homeScores = new int[4];
    private int[] awayScores = new int[4];
    private int[] sbTeam1Scores = new int[4];
    private int[] sbTeam2Scores = new int[4];
    private ArrayList<String> teamSet1 = new ArrayList<>();
    private ArrayList<String> teamSet2 = new ArrayList<>();
    private Random random = new Random();
    private Scanner scanner = new Scanner(System.in);

    public void playGame(boolean postSeason) {
        while (!postSeason) {
            System.out.print("Select a team to play as, using their number: ");
            int homeTeamNum = scanner.nextInt();

            System.out.print("Select the away team: ");
            int awayTeamNum = scanner.nextInt();

            GenericTeam teams = new GenericTeam(homeTeamNum, awayTeamNum);
            teams.team1.setTeam(homeTeamNum);
            teams.team2.setTeam(awayTeamNum);

            addTeams(teams.team1, teams.team2);

            while (quarter < 4) {
                int totalPosessions = random.nextInt(10) + 2;
                while (posession <= totalPosessions) {
                    teams.calcTeam1Play();
                    posession++;
                    teams.calcTeam2Play();
                    posession++;

                    homeScores[quarter] = teams.team1.getScore();
                    awayScores[quarter] = teams.team2.getScore();
                }
                if (quarter == 3) {
                    homeScores[3] += 4;
                }
                quarter++;
            }

            System.out.println("\n");

            if (teams.team1.getScore() > teams.team2.getScore()) {
                calcWins(teams.team1);
            } else if (teams.team2.getScore() > teams.team1.getScore()) {
                calcWins(teams.team2);
            }


            printScores(teams.team1, homeScores);
            printScores(teams.team2, awayScores);

            System.out.println(teams.team1.getTeamName() + ": " + getTotalScore(homeScores));
            System.out.println(teams.team2.getTeamName() + ": " + getTotalScore(awayScores));

            System.out.println("Is it time for post season?");
            postSeason = scanner.nextBoolean();

            if (postSeason) {
                mostWins();
                secondMostWins();
                teams = new GenericTeam(bestTeam, secondBestTeam);
                teams.team1.setTeam(bestTeam);
                teams.team2.setTeam(secondBestTeam);

                addTeams(teams.team1, teams.team2);

                while (quarter <= 4) {
                    int totalPosessions = random.nextInt(10) + 2;
                    while (posession < totalPosessions) {
                        teams.calcTeam1Play();
                        posession++;
                        teams.calcTeam2Play();
                        posession++;

                        sbTeam1Scores[quarter - 1] = teams.team1.getScore();
                        sbTeam2Scores[quarter - 1] = teams.team2.getScore();
                    }
                    quarter++;
                }
            }

            if (postSeason) {
                if (teams.team1.getScore() > teams.team2.getScore()) {
                    System.out.println("Congrats " + teams.team1.getTeamName() + " you won the Super Bowl");
                } else if (teams.team2.getScore() > teams.team1.getScore()) {
                    System.out.println("Congrats " + teams.team2.getTeamName() + " you won the Super Bowl");
                } else {
                    System.out.println("Seems like there was a tie...");
                }

                System.out.println("Overall Team Wins");
                System.out.println("-----------------");
                System.out.println("Bears:" + bearsWins);
                System.out.println("Packers: " + packersWins);
                System.out.println("Vikings: " + vikingsWins);
                System.out.println("Generic: " + genericWins);

                System.out.println("Teams that played:");
                System.out.println(teamSet1);
                System.out.println(teamSet2);
            } else {
                if (teams.team1.getWins() > teams.team2.getWins()) {
                    System.out.println("Congratulations " + teams.team1.getTeamName() + ", you have won: ");
                    System.out.println(teams.team1.getWins() + " games");
                    System.out.println("\n");
                    System.out.println(teams.team2.getTeamName() + ": " + teams.team2.getWins() + " wins");
                } else if (teams.team2.getWins() > teams.team1.getWins()) {
                    System.out.println("Congratulations " + teams.team2.getTeamName() + ", you have won: ");
                    System.out.println(teams.team2.getWins() + " games");
                    System.out.println("\n");
                    System.out.println(teams.team1.getTeamName() + ": " + teams.team1.getWins() + " wins");
                }
            }
        }
    }

    private void calcWins(Team team) {
        if (team.getTeam() == 1) {
            bearsWins++;
        } else if (team.getTeam() == 2) {
            packersWins++;
        } else if (team.getTeam() == 3) {
            vikingsWins++;
        } else {
            genericWins++;
        }
    }

    private int getTotalScore(int nums[]) {
        int totalTeamScore = 0;

        for (int i = 0; i < 4; i++) {
            totalTeamScore += nums[i];
        }

        return totalTeamScore;
    }

    private void addTeams(Team team1, Team team2) {
        teamSet1.add(team1.getTeamName());
        teamSet2.add(team2.getTeamName());
    }

    private void mostWins() { //returns teamNumber
        max = 0;
        if (bearsWins > max) max = bearsWins;
        if (packersWins > max) max = packersWins;
        if (vikingsWins > max) max = vikingsWins;
        if (genericWins > max) max = genericWins;

        if (bearsWins == max) {
            bestTeam = 1;
        } else if (packersWins == max) {
            bestTeam = 2;
        } else if (vikingsWins == max) {
            bestTeam = 3;
        } else {
            bestTeam = 4;
        }
    }

    private void secondMostWins() {
        int secondMax = 0;
        if (bearsWins < max && bearsWins > secondMax) secondMax = bearsWins;
        if (packersWins < max && packersWins > secondMax) secondMax = packersWins;
        if (vikingsWins < max && vikingsWins > secondMax) secondMax = vikingsWins;
        if (genericWins < max && vikingsWins > secondMax) secondMax = genericWins;

        if (bearsWins == secondMax) {
            secondBestTeam = 1;
        } else if (packersWins == secondMax) {
            secondBestTeam = 2;
        } else if (vikingsWins == secondMax) {
            secondBestTeam = 3;
        } else {
            secondBestTeam = 4;
        }
    }

    private void printScores(Team team, int array[]) {
        System.out.println(team.getTeamName());
        System.out.println("Q1 Q2 Q3 Q4");
        for (int i = 0; i <= array.length - 1; i++) {
            System.out.print(array[i] + "  ");
        }
        System.out.print("\n");
    }
}

package com.apex;

import java.util.Random;

public class GenericTeam extends Team {

    //protected int homeTeamNumber, awayTeamNumber;
    int teamNumber1, teamNumber2;
    //Team homeTeam, awayTeam;
    Random random = new Random();

    public GenericTeam(int teamNumber1, int teamNumber2) {
        this.teamNumber1 = teamNumber1;
        this.teamNumber2 = teamNumber2;
    }

    Team team1 = new Team();
    Team team2 = new Team();

    public void calcTeam1Play() {
        int play = random.nextInt(9);
        team1.calculatePlay(play);
    }

    public void calcTeam2Play() {
        int play = random.nextInt(9);
        team2.calculatePlay(play);
    }
}

package com.apex;

public class Team {
    private int team;
    private int score;
    private int wins;

    public Team() {
        this.score = 0;
        this.wins = 0;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int a) {
        wins = wins + a;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team){
        this.team = team;
    }

    public void calculatePlay(int rand) {
        if(team == 1) { //Bears
            if (rand >= 0 && rand <= 2) {
                System.out.println("No Score");
            } else if (rand == 3) {
                System.out.println("Bears score with safety");
                score += 2;
            } else if (rand >= 4 && rand <= 7) {
                System.out.println("Bears score a field goad");
                score += 3;
            } else if (rand == 8 || rand == 9) {
                System.out.println("Bears score a touch down!");
                score += 7;
            }
        }

        if(team == 2) { //Packers
            if (rand >= 0 && rand <= 3) {
                System.out.println("No Score");
            } else if (rand == 4) {
                System.out.println("Packers score with safety");
                score += 2;
            } else if (rand == 5 || rand == 6) {
                System.out.println("Packers score a field goal");
                score += 3;
            } else if (rand >= 7 && rand <= 9) {
                System.out.println("Packers score a touch down!");
                score += 7;
            }
        }

        if(team == 3) { //Vikings
            if (rand == 0 || rand == 1) {
                System.out.println("No Score");
            } else if (rand >= 2 && rand <= 5) {
                System.out.println("Vikings score with safety");
                score += 2;
            } else if (rand == 6 || rand == 7) {
                System.out.println("Vikings score a field goal");
                score += 3;
            } else if (rand == 8 || rand == 9) {
                System.out.println("Packers score a touch down!");
                score += 7;
            }
        }

        if(team == 4) { //Generic Team
            if (rand >= 0 && rand <= 3) {
                System.out.println("No Score");
            } else if (rand == 4 || rand == 5) {
                System.out.println("Generic team score with safety");
                score += 2;
            } else if (rand == 6 || rand == 7) {
                System.out.println("Generic team score a field goal");
                score += 3;
            } else if (rand == 8 || rand == 9) {
                System.out.println("Generic team score a touch down!");
                score += 7;
            }
        }
    }

    public int getScore() {
        return score;
    }

    public String getTeamName() {
        if(team == 1) {
            return "Chicago Bears";
        } else if (team == 2) {
            return "Green Bay Packers";
        } else if (team == 3) {
            return "Minnesota Vikings";
        } else if (team == 4) {
            return "Genaric Team";
        } else {
            return "Error with getting team name, check team number";
        }
    }

}
